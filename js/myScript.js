/**
 * Inicializa o Parallax na página
 */
$(document).ready(function(){
	$('.parallax').parallax();
	$('.scrollspy').scrollSpy();
	$('.carousel').carousel();
	$('.slider').slider();
});